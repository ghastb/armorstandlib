# ArmorStandLib

This is my personal library for working with armorstand modeling and animating.

## Author

* **MikyPro_** - [MikyPro_](https://gitlab.com/MikyPro_)

# TUTORIAL
From (https://www.spigotmc.org/threads/tutorial-how-to-use-armorstandlib.363292/).

        **INTRODUCTION**
        First of all, ArmorStandLib(https://www.spigotmc.org/resources/api-lib-armorstandlib-a-full-armor-stand-library.65494/) is a library for modeling and animating armorstands with ease.
        To work with it in a good way you have to know a little the math additions, subtractions, multiplications, divisions and the vectors (and the java basics obviously).

        **TUTORIAL**
        The text builder:

        the textbuilder method is TextBuilder(java.lang.String, org.bukkit.Location), the java.lang.String is a text object in java like this "Hello World". org.bukkit.Location in this case is like a 3d vector represented in this way ( v(1(direction1(on mc x)),2(height(on mc y)),3(direction2(on mc z))) ). You can create a TextBuilder using this code
        Code (Text):
        new TextBuilder("MIKYPRO", new Location(0, 5, 0));
        .

        Now the less detailed part dedicated to developers with a minimum of experience.

        (all the animations ends with two longs start after and stop after, start after is the time in ticks that the animation wait before it starts, stop after is the time in ticks that the animation wait before it ends).

        The around axis rotation (AroundAxisRotation.java) it has one constructor ( AroundAxisRotation(ArmorStand stand, Location location, float radius, float radPerSec, long start_after, long stop_after) ) radPerSec is the percentage of radius (0 to 10) the armorstand does in one second, radius is the radius of distance from the location point and the armorstand.

        The articulation move (ArticulationMove.java) COMING SOON... (it is still in developing for now).

        The ascend and the descend (Ascend.java and Descend.java) these classes are 95% equals. The constructor is (ArmorStand stand, long start_after, long stop_after) you simply have to compile it and the armorstand starts ascending or descending.

        The axis rotation (AxisRotation.java) it has one constructor (ArmorStand stand, Sense sense, long start_after, long stop_after) Sense.java is an enum with these two instances Sense.RIGHT and Sense.LEFT. If you insert Sense.RIGHT it will rotate clockwise, if you insert Sense.LEFT it will rotate non-clockwise.

        The directional walk (DirectionWalk.java) it has one constructor (ArmorStand stand, Direction direction, long start_after, long stop_after) Direction.java is an enum that represent the cardinal direction system in minecraft you can set it with its values (Direction.NORTH, Direction.SOUTH, Direction.EAST, Direction.WEST) or get it using Direction.CARDINAL or Direction.getCardinalDirection(org.bukkit.entity.LivingEntity).

        Thats the end for now (09,03,2019) i will update this page if i add new features. ;) (PS. Sorry for the little grammar issues i am italian :p)

## License

This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. - see the [LICENSE.md](LICENSE.md) file for details.
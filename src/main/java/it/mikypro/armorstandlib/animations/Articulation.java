package it.mikypro.armorstandlib.animations;

public enum Articulation {
    RIGHT_HAND,
    LEFT_HAND,
    RIGHT_LEG,
    LEFT_LEG
}

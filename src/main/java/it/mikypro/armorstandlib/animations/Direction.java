package it.mikypro.armorstandlib.animations;

import org.bukkit.entity.LivingEntity;

public enum  Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST,
    CARDINAL;

    public static Direction getCardinalDirection(LivingEntity entity) {
        float yaw = entity.getLocation().getYaw();
        if (yaw < 0) {
            yaw += 360;
        }
        if (yaw >= 315 || yaw < 45) {
            return Direction.SOUTH;
        } else if (yaw < 135) {
            return Direction.WEST;
        } else if (yaw < 225) {
            return Direction.NORTH;
        } else if (yaw < 315) {
            return Direction.EAST;
        }
        return Direction.NORTH;
    }
}

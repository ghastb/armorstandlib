package it.mikypro.armorstandlib.animations.types;

import it.mikypro.armorstandlib.ArmorStandLib;
import it.mikypro.armorstandlib.animations.Articulation;
import lombok.Getter;
import org.bukkit.entity.ArmorStand;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.EulerAngle;
//TODO> Sistemare il sistema di movimento. (fix the movement system)
public class ArticulationMove extends BukkitRunnable {

    @Getter
    private Articulation articulation;
    @Getter
    private EulerAngle angle;
    @Getter
    private ArmorStand stand;

    private boolean xEquals = false, yEquals = false, zEquals = false;
    private EulerAngle articulationAngle;

    public ArticulationMove(ArmorStand stand, Articulation articulation, EulerAngle angle, long start_after, long stop_after) {
        this.stand = stand;
        this.articulation = articulation;
        this.angle = angle;
        articulationAngle = stand.getRightArmPose();

        BukkitRunnable animation = this;

        new BukkitRunnable() {
            @Override
            public void run() {
                animation.runTaskTimer(ArmorStandLib.getInstance(), 0, 1);
            }
        }.runTaskLaterAsynchronously(ArmorStandLib.getInstance(), start_after);

        if (stop_after < 0) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                animation.cancel();
            }
        }.runTaskLaterAsynchronously(ArmorStandLib.getInstance(), stop_after);
    }

    public void run() {

        if (xEquals && yEquals && zEquals) {
            this.cancel();
            return;
        }

        if (articulation == Articulation.RIGHT_HAND) {
            articulationAngle = stand.getRightArmPose();
        } else if (articulation == Articulation.RIGHT_LEG) {
            articulationAngle = stand.getRightLegPose();
        } else if (articulation == Articulation.LEFT_HAND) {
            articulationAngle = stand.getLeftArmPose();
        } else if (articulation == Articulation.LEFT_LEG) {
            articulationAngle = stand.getLeftLegPose();
        }


        if (angle.getX() == articulationAngle.getX()) xEquals = true;
        if (angle.getY() == articulationAngle.getY()) yEquals = true;
        if (angle.getZ() == articulationAngle.getZ()) zEquals = true;

        if (!xEquals) {
            if (angle.getX() - 1 > articulationAngle.getX()) {
                articulationAngle = articulationAngle.subtract(.001,0,0);
            }

            if (angle.getX() + 1 < articulationAngle.getX()) {
                articulationAngle = articulationAngle.add(.001,0,0);
            }
        }

        if (!yEquals) {
            if (angle.getY() - 1 > articulationAngle.getY()) {
                articulationAngle = articulationAngle.subtract(0,.001,0);
            }

            if (angle.getY() + 1 < articulationAngle.getY()) {
                articulationAngle = articulationAngle.add(0,.001,0);
            }
        }

        if (!zEquals) {
            if (angle.getZ() - 1 > articulationAngle.getZ()) {
                articulationAngle = articulationAngle.subtract(0,0,.001);
            }

            if (angle.getZ() + 1 < articulationAngle.getZ()) {
                articulationAngle = articulationAngle.add(0,0,.001);
            }
        }

        if (articulation == Articulation.RIGHT_HAND) {
            stand.setRightArmPose(articulationAngle);
        } else if (articulation == Articulation.RIGHT_LEG) {
            stand.setRightLegPose(articulationAngle);
        } else if (articulation == Articulation.LEFT_HAND) {
            stand.setLeftArmPose(articulationAngle);
        } else if (articulation == Articulation.LEFT_LEG) {
            stand.setLeftLegPose(articulationAngle);
        }
    }
}

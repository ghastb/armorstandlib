package it.mikypro.armorstandlib.animations.types;

import it.mikypro.armorstandlib.ArmorStandLib;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class AroundAxisRotation extends BukkitRunnable {

    @Getter
    private ArmorStand stand;

    private Location location;
    private float radius;
    private float radPerTick;
    private int tick = 0;

    public AroundAxisRotation(ArmorStand stand, Location location, float radius, float radPerSec, long start_after, long stop_after) {
        this.stand = stand;
        this.location = location;
        this.radius = radius;
        this.radPerTick = radPerSec / 20f;

        BukkitRunnable animation = this;

        new BukkitRunnable() {
            @Override
            public void run() {
                animation.runTaskTimer(ArmorStandLib.getInstance(), 0, 1);
            }
        }.runTaskLaterAsynchronously(ArmorStandLib.getInstance(), start_after);

        if (stop_after < 0) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                animation.cancel();
            }
        }.runTaskLaterAsynchronously(ArmorStandLib.getInstance(), stop_after);
    }

    public void run() {
        ++tick;

        Location loc = getLocationAroundCircle(location.clone(), radius, radPerTick * tick);
        stand.setVelocity(new Vector(1, 0, 0));
        stand.teleport(loc);
    }

    /*
    Code by https://github.com/CoKoc
     */
    private Location getLocationAroundCircle(Location center, double radius, double angleInRadian) {
        double x = center.getX() + radius * Math.cos(angleInRadian);
        double z = center.getZ() + radius * Math.sin(angleInRadian);
        double y = center.getY();

        Location loc = new Location(center.getWorld(), x, y, z);
        Vector difference = center.toVector().clone().subtract(loc.toVector());
        loc.setDirection(difference);

        return loc;
    }
}

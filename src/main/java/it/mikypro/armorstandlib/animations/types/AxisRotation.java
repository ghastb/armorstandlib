package it.mikypro.armorstandlib.animations.types;

import it.mikypro.armorstandlib.ArmorStandLib;
import it.mikypro.armorstandlib.animations.Sense;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.scheduler.BukkitRunnable;

public class AxisRotation extends BukkitRunnable {

    @Getter
    private Sense sense;
    @Getter
    private ArmorStand stand;

    public AxisRotation(ArmorStand stand, Sense sense, long start_after, long stop_after) {
        this.stand = stand;
        this.sense = sense;

        BukkitRunnable animation = this;

        new BukkitRunnable() {
            @Override
            public void run() {
                animation.runTaskTimer(ArmorStandLib.getInstance(), 0, 1);
            }
        }.runTaskLaterAsynchronously(ArmorStandLib.getInstance(), start_after);

        if (stop_after < 0) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                animation.cancel();
            }
        }.runTaskLaterAsynchronously(ArmorStandLib.getInstance(), stop_after);
    }

    private double i=0;

    @Override
    public void run() {
        if (sense.equals(Sense.RIGHT)) {
            i += 10;
        } else {
            i -= 10;
        }
        stand.teleport(new Location(stand.getLocation().getWorld(), stand.getLocation().getX(), stand.getLocation().getY(), stand.getLocation().getZ(), (float) i, stand.getLocation().getPitch()));
    }
}

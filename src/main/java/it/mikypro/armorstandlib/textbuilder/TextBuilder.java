package it.mikypro.armorstandlib.textbuilder;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

public class TextBuilder {

    @Getter
    private String string;
    @Getter
    private Location location;
    @Getter
    private double ia=0.62,ib=0.62,ic=2;

    public TextBuilder(String string, Location location) {
        this.string = string;
        location.setPitch(0);
        location.setYaw(180);
        this.location = location;
        build();
    }

    public TextBuilder(String string, Location location, double ia, double ib, double ic) {
        this.string = string;
        location.setPitch(0);
        location.setYaw(180);
        this.location = location;
        this.ia = ia;
        this.ib = ib;
        this.ic = ic;
        build();
    }

    private void build() {
        char[] chars = string.replace(" ", "_").toCharArray();
        int z = 0;
        for (char c : chars) {
            String cc = c + "";
            Shape shape = Shape.valueOf(isInteger(cc) ? "N"+cc : cc.toUpperCase());
            double y = ib*5;
            for (String st : shape.getStrings()) {
                double x= 0;
                char[] chars1s = st.toCharArray();
                for (char cs : chars1s) {
                    String ccs = cs + "";
                    if (ccs.equals("#")) {
                        box(location.clone().add(z+x,y,0));
                    }
                    x += ia;
                }
                y -= ib;
            }
            z += ic;
        }
    }

    private void box(Location spawnLoc) {
        ArmorStand stand = (ArmorStand) location.getWorld().spawnEntity(spawnLoc, EntityType.ARMOR_STAND);
        stand.setGravity(false);
        stand.setVisible(false);
        stand.setHelmet(new ItemStack(Material.DIAMOND_BLOCK, 1));
    }

    private boolean isInteger(String string) {
        try{
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

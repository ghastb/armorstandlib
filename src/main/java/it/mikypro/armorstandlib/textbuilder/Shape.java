package it.mikypro.armorstandlib.textbuilder;

import lombok.Getter;

public enum Shape {
    _("   "),
    A("###", "# #", "###", "# #", "# #"),
    B("## ", "# #", "## ", "# #", "## "),
    C("###", "#  ", "# ", "# ", "###"),
    D("## ", "# #", "# #", "# #", "## "),
    E("###", "#  ", "###", "#  ", "###"),
    F("###", "#  ", "###", "#  ", "#  "),
    G("###", "#  ", "# ", "# #", "###"),
    H("# #", "# #", "###", "# #", "# #"),
    I("###", " # ", " # ", " # ", "###"),
    J(" ##", "  #", "  #", "# #", "###"),
    K("# #", "# #", "## ", "# #", "# #"),
    L("#  ", "#  ", "#  ", "#  ", "###"),
    M("# #", "###", "# #", "# #", "# #"),
    N("## ", "# #", "# #", "# #", "# #"),
    O("###", "# #", "# #", "# #", "###"),
    P("## ", "# #", "##", "#  ", "#  "),
    Q("###", "# #", "# #", "# #", "###", "   #"),
    R("###", "# #", "###", "## ", "# #"),
    S("###", "#  ", "###", "  #", "###"),
    T("###", " # ", " # ", " # ", " # "),
    U("# #", "# #", "# #", "# #", "###"),
    V("# #", "# #", "# #", "# #", " # "),
    W("# #", "# #", "# #", "###", "# #"),
    X("# #", "# #", " # ", "# #", "# #"),
    Y("# #", "# #", " # ", " # ", " # "),
    Z("###", "  #", " # ", "#  ", "###"),
    N0("###", "# #", "# #", "# #", "###"),
    N1("## ", " # ", " # ", " # ", "###"),
    N2("###", "# #", " # ", "#  ", "###"),
    N3("###", "  #", " # ", "  #", "###"),
    N4(" ##", "# #", "###", "  #", "  #"),
    N5("###", "#  ", "###", "  #", "###"),
    N6("###", "#  ", "###", "# #", "###"),
    N7("###", "  #", " # ", "#  ", "#  "),
    N8("###", "# #", "###", "# #", "###"),
    N9("###", "# #", "###", "  #", " ##");

    @Getter
    private String[] strings;

    Shape(String... strings) {
        this.strings = strings;
    }
}
